OMEGAlpes Structure
====================

A detailed presentation of OMEGAlpes in the PhD report of Camille Pajot is available in French here:
`OMEGAlpes Outil d'aide à la décision pour une planification énergétique multi-fluides optimale à l'échelle des quartiers`_

The models are bases on **general**, **energy** and **actor** classes.
The structure of the library is described on the following class diagram:

.. figure::  images/class_diagram.png
   :align:   center
   :scale:   40%

   *Figure: OMEGAlpes class diagram*

The energy units are detailed in the energy package

.. toctree::
   :maxdepth: 2

   api/energy_package

The **actor classes** enables to build the energy model considering pre-defined
stakeholders' constraints and objectives

.. toctree::
   :maxdepth: 2

   api/actor

The **general classes** helps to build the units, the model and to plot the results

.. toctree::
   :maxdepth: 2

   api/general

.. _OMEGAlpes Outil d'aide a la decision pour une planification energetique multi-fluides optimale à l'echelle des quartiers: https://www.theses.fr/s162247