Welcome to OMEGAlpes' documentation!
=====================================


Introduction to OMEGAlpes
--------------------------

OMEGAlpes stands for Generation of Optimization Models As Linear Programming for Energy Systems.
It aims to be an energy systems modelling tool for linear optimisation (LP, MILP).
It is currently based on the LP modeler PuLP.

It is an Open Source project located on GitLab at `OMEGAlpes Gitlab`_


Contents
--------
.. toctree::
   :maxdepth: 1

   installation_requirements

.. toctree::
   :maxdepth: 2

   OMEGAlpes_description

.. toctree::
   :maxdepth: 1

   OMEGAlpes_grah_representation

.. toctree::
   :maxdepth: 1

   new_functionalities

.. toctree::
   :maxdepth: 1

   examples


Acknowledgments
---------------
Vincent Reinbold - Library For Linear Modeling of Energetic Systems : https://github.com/ReinboldV

Mathieu Brugeron

This work has been partially supported by the `CDP Eco-SESA`_ receiving fund from the French National Research Agency
in the framework of the "Investissements d’avenir” program (ANR-15-IDEX-02) and the VALOCAL project
(CNRS Interdisciplinary Mission and INSIS)

.. _CDP Eco-SESA: https://ecosesa.univ-grenoble-alpes.fr/
.. _OMEGAlpes Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes
.. _OMEGAlpes Examples Documentation: https://omegalpes_examples.readthedocs.io/
