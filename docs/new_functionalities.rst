What's new in the latest versions
=================================

The new version of OMEGAlpes v0.3.01 is available!
The release if from 27th March 2020.

.. toctree::
   :maxdepth: 2

   new_functionalities/v0.3.1


Information about the latest versions
-------------------------------------

.. toctree::
   :maxdepth: 1

   new_functionalities/v0.3.0
