What’s new in version 0.3.1
===========================
Version available since 13th March 2020

Bug fixed
---------

- Change remaining pmin and pmax parameters into p_min and p_max of
  EnergyUnits in conversion units
- Add verbose parameter set to False in
  general/utils/input_data/resample_data() Timeunit
- Change expected type of *units in connect_units() from list to EnergyUnit


New functionalities
-------------------

General
+++++++

Time
****
- Add verbose parameter added to TimeUnit in order to be able not to print the
  studied time period.

Elements
********
- Add three classes to make a distinction between different kind of
  constraints: DefinitionConstraint, TechnicalConstraints and ActorConstraint.
  Considering the DynamicConstraints, three other classes have been created:
   * DefinitionDynamicConstraint,
   * TechnicalDynamicConstraints, and
   * ActorDynamicConstraint.


Deprecated
----------

Class
+++++

- Deprecate ExternalConstraint and ExtDynConstraint in module Elements


Contributors
------------

Lou Morriet,
Sacha Hodencq
