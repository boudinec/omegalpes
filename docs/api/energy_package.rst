energy package
==============

The energy package gathers the energy units, poles and nodes modules:

.. contents::
    :depth: 1
    :local:
    :backlinks: top

The energy units inherit from the :class:`~omegalpes.energy.units.energy_units.EnergyUnit` object which
itself inherit from the :class:`~omegalpes.general.optimisation.units.Unit` object.

Energy_units module
-------------------

.. automodule:: omegalpes.energy.units.energy_units
    :members:
    :undoc-members:
    :show-inheritance:

Consumption_units module
------------------------

.. automodule:: omegalpes.energy.units.consumption_units
    :members:
    :undoc-members:
    :show-inheritance:

Production_units module
-----------------------

.. automodule:: omegalpes.energy.units.production_units
    :members:
    :undoc-members:
    :show-inheritance:

Conversion_units module
-----------------------

.. automodule:: omegalpes.energy.units.conversion_units
    :members:
    :undoc-members:
    :show-inheritance:

Storage_units module
--------------------

.. automodule:: omegalpes.energy.units.storage_units
    :members:
    :undoc-members:
    :show-inheritance:

Reversible_units module
-----------------------

.. automodule:: omegalpes.energy.units.reversible_units
    :members:
    :undoc-members:
    :show-inheritance:

Energy_nodes module
-------------------

.. automodule:: omegalpes.energy.energy_nodes
    :members:
    :undoc-members:
    :show-inheritance:

Thermal Building module
-----------------------

.. automodule:: omegalpes.energy.buildings.thermal
    :members:
    :undoc-members:
    :show-inheritance:

Exergy module
-----------------------

.. automodule:: omegalpes.energy.exergy
    :members:
    :undoc-members:
    :show-inheritance:

Poles module
------------

.. automodule:: omegalpes.energy.io.poles
    :members:
    :undoc-members:
    :show-inheritance:
