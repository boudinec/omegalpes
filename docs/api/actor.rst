actor package
==============

The actor modelling is based on a main actor class defined on the actor
module.
Then, the actors are divided in two categories: the "operator actors" who
operates energy units and the "regulator actors" who unable to create
regulation constraints.

.. contents::
    :depth: 1
    :local:
    :backlinks: top

Actor module
------------

.. automodule:: omegalpes.actor.actor
    :members:
    :undoc-members:
    :show-inheritance:

Operator_actors module
----------------------

.. automodule:: omegalpes.actor.operator_actors.operator_actors
    :members:
    :undoc-members:
    :show-inheritance:

Consumer_actors module
----------------------

.. automodule:: omegalpes.actor.operator_actors.consumer_actors
    :members:
    :undoc-members:
    :show-inheritance:

Producer_actors module
----------------------

.. automodule:: omegalpes.actor.operator_actors.producer_actors
    :members:
    :undoc-members:
    :show-inheritance:

Consumer_producer_actors module
-------------------------------

.. automodule:: omegalpes.actor.operator_actors.consumer_producer_actors
    :members:
    :undoc-members:
    :show-inheritance:

Regulator_actors module
-----------------------

.. automodule:: omegalpes.actor.regulator_actors.regulator_actors
    :members:
    :undoc-members:
    :show-inheritance:
