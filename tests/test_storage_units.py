#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
Unit tests for the module storage_units.py, defining the storage units
attributes, constraints and functions.

..

    Copyright 2018 G2ELab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import unittest
from omegalpes.energy.units.storage_units import *
from omegalpes.general.time import TimeUnit
from pulp import LpBinary
from omegalpes.general.optimisation.elements import DynamicConstraint, \
    TechnicalConstraint, Constraint
from omegalpes.general.optimisation.elements import Objective


class TestStorageUnitInit(unittest.TestCase):
    """Checking the initialisation of the StorageUnit class"""

    def setUp(self):
        self.su0 = StorageUnit(time=TimeUnit(periods=4, dt=1), name='STU',
                               capacity=10)

    def test_SOC_error(self):
        """Asserting an error is raised when SOCmin > SOCmax"""
        with self.assertRaises(ValueError):
            suErr = StorageUnit(time=TimeUnit(periods=24, dt=1), name='STU',
                                soc_min=1e+3, soc_max=5e+2)

    def test_default_values_capacity(self):
        """Checking the default values for the capacity Quantity"""
        self.assertEqual(self.su0.capacity.name, 'capacity')
        self.assertEqual(self.su0.capacity.unit, 'kWh')
        self.assertEqual(self.su0.capacity.value, 10)  # set to 0 in Quantity
        self.assertEqual(self.su0.capacity.lb, 0)
        self.assertEqual(self.su0.capacity.vlen, 1)

    def test_default_values_energy(self):
        """Checking the default values for the energy Quantity"""
        self.assertEqual(self.su0.e.name, 'e')
        self.assertEqual(self.su0.e.opt, {0: True, 1: True, 2: True, 3: True})
        self.assertEqual(self.su0.e.description, 'energy at t in the storage')
        self.assertEqual(self.su0.e.unit, 'kWh')
        self.assertEqual(self.su0.e.vlen, 4)
        self.assertEqual(self.su0.e.lb, 0)
        self.assertEqual(self.su0.e.ub, 10)

    def test_default_values_pc(self):
        """Checking the default values for the pc Quantity"""
        self.assertEqual(self.su0.pc.name, 'pc')
        self.assertEqual(self.su0.pc.opt, {0: True, 1: True, 2: True, 3: True})
        self.assertEqual(self.su0.pc.unit, 'kW')
        self.assertEqual(self.su0.pc.vlen, 4)
        self.assertEqual(self.su0.pc.lb, 0)
        self.assertEqual(self.su0.pc.ub, 1e+5)

    def test_default_values_pd(self):
        """Checking the default values for the pd Quantity"""
        self.assertEqual(self.su0.pd.name, 'pd')
        self.assertEqual(self.su0.pd.opt, {0: True, 1: True, 2: True, 3: True})
        self.assertEqual(self.su0.pd.unit, 'kW')
        self.assertEqual(self.su0.pd.vlen, 4)
        self.assertEqual(self.su0.pd.lb, 0)
        self.assertEqual(self.su0.pd.ub, 1e+5)

    def test_default_values_uc(self):
        """Checking the default values for the uc Quantity"""
        self.assertEqual(self.su0.uc.name, 'uc')
        self.assertEqual(self.su0.uc.vtype, LpBinary)
        self.assertEqual(self.su0.uc.vlen, 4)
        self.assertEqual(self.su0.uc.opt, {0: True, 1: True, 2: True, 3: True})
        self.assertEqual(self.su0.uc.description, 'binary variable 0:No '
                                                  'charging & 1:charging')


class TestSOCStorageUnits(unittest.TestCase):
    def setUp(self):
        self.suFloat = StorageUnit(name='suf', time=TimeUnit(periods=3, dt=1),
                                   soc_min=0.2, soc_max=0.9)
        self.suList = StorageUnit(name='sul', time=TimeUnit(periods=3, dt=1),
                                  soc_min=[0, 0.5, 0.3], soc_max=[1, 1, 0.8])

    def test_SOC_float(self):
        """Checking the SOC is properly set when it is float"""
        self.assertEqual(self.suFloat.set_soc_min.exp_t, '{0}_e[t] >= {1} * '
                                                         '{0}_capacity'.format(
            'suf', 0.2))
        self.assertEqual(self.suFloat.set_soc_max.exp_t, '{0}_e[t] <= {1} * '
                                                         '{0}_capacity'.format(
            'suf', 0.9))

    def test_SOC_list(self):
        """Checking the SOC is properly set when it is float"""
        self.assertEqual(self.suList.set_soc_min.exp_t, '{0}_e[t] >= {1}[t] '
                                                        '* {0}_capacity'
                         .format('sul', [0, 0.5, 0.3]))
        self.assertEqual(self.suList.set_soc_max.exp_t, '{0}_e[t] <= {1}[t] * '
                                                        '{0}_capacity'.format(
            'sul', [1, 1, 0.8]))


class TestSelfDisch(unittest.TestCase):
    def setUp(self):
        self.suSelf = StorageUnit(name='su_self', time=TimeUnit(periods=4,
                                                                dt=1),
                                  capacity=10, self_disch=1e-3,
                                  self_disch_t=2e-3)

    def test_calc_e(self):
        """Checking the energy calculation is properly set"""
        self.assertEqual(self.suSelf.calc_e.name, 'calc_e')
        self.assertEqual(self.suSelf.calc_e.t_range, 'for t in time.I[:-1]')
        self.assertEqual(self.suSelf.calc_e.exp_t,
                         '{0}_e[t+1] - {0}_e[t]*(1-{1}*time.DT)'
                         ' - time.DT * ({0}_pc[t]*{3}- '
                         '{0}_pd[t]*1/{4}- {2}*'
                         '{0}_capacity) == 0'
                         .format('su_self', 2e-3,
                                 1e-3, 1, 1))

    def test_wrong_self_disch_value(self):
        """Asserting an error is raised when the self_disch value is over 1"""
        with self.assertRaises(ValueError):
            suWrongSelf = StorageUnit(time=TimeUnit(periods=4, dt=1),
                                      name='STU',
                                      capacity=10, self_disch=1e+2)

    def test_wrong_self_disch_t_value(self):
        """Asserting an error is raised when the self_disch_t value is over
        1"""
        with self.assertRaises(ValueError):
            suWrongSelf = StorageUnit(time=TimeUnit(periods=4, dt=1),
                                      name='STU',
                                      capacity=10, self_disch_t=1e+2)


class TestCalcP(unittest.TestCase):
    def setUp(self):
        self.suCalcP = StorageUnit(name='su_calcP', time=TimeUnit(periods=4,
                                                                  dt=1))

    def test_calc_p(self):
        """Checking the power calculation is properly set"""
        self.assertEqual(self.suCalcP.calc_p.exp_t, '{0}_p[t] == {0}_pc[t] - '
                                                    '{0}_pd[t]'
                         .format('su_calcP'))
        self.assertEqual(self.suCalcP.calc_p.name, 'calc_p')
        self.assertEqual(self.suCalcP.calc_p.t_range, 'for t in time.I')


class TestOnOffStor(unittest.TestCase):
    def setUp(self):
        self.suoos = StorageUnit(name='su_oos', time=TimeUnit(periods=4, dt=1))

    def test_on_off_stor(self):
        """Checking the on_off_stor is properly set"""
        self.assertEqual(self.suoos.on_off_stor.name, 'on_off_stor')
        self.assertEqual(self.suoos.on_off_stor.t_range, 'for t in time.I')
        self.assertEqual(self.suoos.on_off_stor.exp_t, '{0}_pc[t] + {0}_pd['
                                                        't] - {0}_u[t] * {1}'
                                                        ' >= 0'
                         .format('su_oos', 0.001))


class TestMaxMinCharging(unittest.TestCase):
    def setUp(self):
        self.suc = StorageUnit(name='su_c', time=TimeUnit(periods=4, dt=1))

    def test_max_charging(self):
        """Checking the max_charging constraint"""
        self.assertEqual(self.suc.def_max_charging.name, 'def_max_charging')
        self.assertEqual(self.suc.def_max_charging.t_range, 'for t in time.I')
        self.assertEqual(self.suc.def_max_charging.exp_t, '{0}_pc[t] - '
                                                          '{0}_uc[t] * {1} '
                                                          '<= 0'
                         .format('su_c', 1e+5))

    def test_min_charging(self):
        """Checking the min_charging constraint"""
        self.assertEqual(self.suc.def_min_charging.name, 'def_min_charging')
        self.assertEqual(self.suc.def_min_charging.t_range, 'for t in time.I')
        self.assertEqual(self.suc.def_min_charging.exp_t, '{0}_pc[t] - {'
                                                          '0}_uc[t] * {1} >= 0'
                         .format('su_c', 1e-5))


class TestMaxMinDischarging(unittest.TestCase):
    def setUp(self):
        self.sud = StorageUnit(name='su_d', time=TimeUnit(periods=4, dt=1))

    def test_max_discharging(self):
        """Checking the max_discharging constraint"""
        self.assertEqual(self.sud.def_max_discharging.name,
                         'def_max_discharging')
        self.assertEqual(self.sud.def_max_discharging.t_range, 'for t in '
                                                               'time.I')
        self.assertEqual(self.sud.def_max_discharging.exp_t, '{0}_pd[t] - ('
                                                             '1 - {0}_uc['
                                                             't]) * {1} <= 0'
                         .format('su_d', 1e+5))

    def test_min_discharging(self):
        """Checking the min_discharging constraint"""
        self.assertEqual(self.sud.def_min_discharging.name,
                         'def_min_discharging')
        self.assertEqual(self.sud.def_min_discharging.t_range, 'for t in '
                                                               'time.I')
        self.assertEqual(self.sud.def_min_discharging.exp_t, '{0}_pd[t] + ({'
                                                             '0}_uc[t] - {'
                                                             '0}_u[t]) * {1} '
                                                             '>= 0'.format(
            'su_d', 1e-5))


class Teste0eFNone(unittest.TestCase):
    def setUp(self):
        self.sueNone = StorageUnit(name='su_e_none', time=TimeUnit(periods=4,
                                                                   dt=1))

    def test_none_e0(self):
        """Asserting an error is raised when the set_e_0 method is used
        while e_0 is set to None"""
        with self.assertRaises(AttributeError):
            self.sueNone.set_e_0

    def test_none_eF(self):
        """Asserting an error is raised when the set_e_f method is used
        while e_f is set to None"""
        with self.assertRaises(AttributeError):
            self.sueNone.set_e_f


class Test_e_f(unittest.TestCase):
    def setUp(self):
        self.time =TimeUnit(periods=3, dt=1)
        self.suFloat_ef = StorageUnit(name='sueff', e_f=5, capacity=10,
                                      time=TimeUnit(periods=3, dt=1),
                                      soc_min=0.2, soc_max=0.9)
        self.suList_ef = StorageUnit(name='suefl', e_f=5, capacity=10,
                                     time=TimeUnit(periods=3, dt=1),
                                    soc_min=[0, 0.5, 0.3], soc_max=[1, 1, 0.8])
        self.suFloat_no_ef = StorageUnit(name='sunoeff', capacity=10,
                                      time=TimeUnit(periods=3, dt=1),
                                      soc_min=0.2, soc_max=0.9)
        self.suList_no_ef = StorageUnit(name='sunoefl', capacity=10,
                                     time=TimeUnit(periods=3, dt=1),
                                    soc_min=[0, 0.5, 0.3], soc_max=[1, 1, 0.8])

    def test_e_f_SOC_float(self):
        """Checking e_f boundary constraints are properly set when floats"""
        self.assertEqual(self.suFloat_ef.e_f_min.exp, '{0}_e_f >= {1} * '
                                                         '{0}_capacity'.format(
            'sueff', 0.2))
        self.assertEqual(self.suFloat_ef.e_f_max.exp, '{0}_e_f <= {1} * '
                                                         '{0}_capacity'.format(
            'sueff', 0.9))

    def test_e_f_SOC_list(self):
        """Checking boundary constraints are properly set when lists"""
        self.assertEqual(self.suList_ef.e_f_min.exp, '{0}_e_f >= {1}[{2}] '
                                                        '* {0}_capacity'
                         .format('suefl', [0, 0.5, 0.3], self.time.I[-1]))
        self.assertEqual(self.suList_ef.e_f_max.exp, '{0}_e_f <= {1}[{2}] * '
                                                        '{0}_capacity'.format(
            'suefl', [1, 1, 0.8], self.time.I[-1]))

    def test_set_e_f(self):
        """Checking the set_e_f is properly set"""
        self.assertEqual(self.suFloat_ef.set_e_f.name, 'set_e_f')
        self.assertEqual(self.suFloat_ef.set_e_f.exp, '{0}_e_f-{0}_e[{1}] == '
                                                      '{2}*({0}_pc[{1}]*{3}-'
                     '{0}_pd[{1}]*1/{4}-{5}*{0}_e[{1}]-{6}*{0}_capacity)'
                 .format('sueff', self.time.I[-1], 1, 1, 1, 0, 0))

    def test_no_e_f_SOC_float(self):
        """Checking e_f boundary constraints are properly set when floats"""
        self.assertEqual(self.suFloat_no_ef.e_f_min.exp, '{0}_e_f >= {1} * '
                                                         '{0}_capacity'.format(
            'sunoeff', 0.2))
        self.assertEqual(self.suFloat_no_ef.e_f_max.exp, '{0}_e_f <= {1} * '
                                                         '{0}_capacity'.format(
            'sunoeff', 0.9))

    def test_no_e_f_SOC_list(self):
        """Checking boundary constraints are properly set when lists"""
        self.assertEqual(self.suList_no_ef.e_f_min.exp, '{0}_e_f >= {1}[{2}] '
                                                        '* {0}_capacity'
                         .format('sunoefl', [0, 0.5, 0.3], self.time.I[-1]))
        self.assertEqual(self.suList_no_ef.e_f_max.exp, '{0}_e_f <= {1}[{2}] * '
                                                        '{0}_capacity'.format(
            'sunoefl', [1, 1, 0.8], self.time.I[-1]))

    def test_calc_e_f(self):
        """Checking the calc_e_f is properly set"""
        self.assertEqual(self.suFloat_no_ef.calc_e_f.name, 'calc_e_f')
        self.assertEqual(self.suFloat_no_ef.calc_e_f.exp, '{0}_e_f-{0}_e[{1}] '
                                                        '== {2}*({0}_pc[{1}]*'
                                                         '{3}-{0}_pd[{1}]*1/'
                                                         '{4}-{5}*{0}_e[{1}]-'
                                                         '{6}*{0}_capacity)'
                 .format('sunoeff', self.time.I[-1], 1, 1, 1, 0, 0))

class Teste0eF(unittest.TestCase):
    def setUp(self):
        self.time = TimeUnit(periods=4, dt=1)
        self.sue = StorageUnit(name='su_e', time=self.time,
                               e_0=1e+2, e_f=5e+2)

    def test_e0(self):
        """Checking set_e_0 is properly set"""
        self.assertIsInstance(self.sue.set_e_0, ActorConstraint)
        self.assertEqual(self.sue.set_e_0.name, 'set_e_0')
        self.assertEqual(self.sue.set_e_0.exp, '{0}_e[0] == {1}'
                         .format('su_e', 1e+2))

    def test_ef(self):
        """Checking set_e_f is properly set"""
        self.assertIsInstance(self.sue.set_e_f, Constraint)
        self.assertEqual(self.sue.set_e_f.name, 'set_e_f')
        self.assertEqual(self.sue.set_e_f.exp, '{0}_e_f-{0}_e[{1}] == {3}*('
                                               '{0}_pc[{1}]*{4}-{0}_pd[{1}]*'
                                               '1/{5}-{6}*{0}_e[{1}]-{7}*'
                                               '{0}_capacity)'
                         .format('su_e', self.time.I[-1], 0, 1, 1, 1, 0, 0))


class TestE0equalEf(unittest.TestCase):
    def test_e0_eq_ef(self):
        """Checking ef_is_e0 is properly set"""
        sue0eq = StorageUnit(name='su_e0eq', time=TimeUnit(periods=4, dt=1),
                             e_f=5e+2, ef_is_e0=True)
        self.assertIsInstance(sue0eq.ef_is_e0, ActorConstraint)
        self.assertEqual(sue0eq.ef_is_e0.name, 'ef_is_e0')
        self.assertEqual(sue0eq.ef_is_e0.exp, '{0}_e[0] == {0}_e_f'
                         .format('su_e0eq'))

    def test_e0_eq_ef_error(self):
        """Asserting e0_is_e0 raises an error when e_0 and e_f are set to
        different values"""
        with self.assertRaises(ValueError):
            sue0eqerror = StorageUnit(name='su_e0eq', time=TimeUnit(periods=4,
                                                                    dt=1),
                                      e_0=1e+2, e_f=5e+2, ef_is_e0=True)


class TestCyclesNone(unittest.TestCase):
    def test_cycles_none(self):
        """Asserting an error is raised when set_cycles is used while cycles
        is not defined"""
        sucyclesNone = StorageUnit(name='su_cyclesNone', time=TimeUnit(
            periods=4, dt=1))
        with self.assertRaises(AttributeError):
            self.sucyclesNone.set_cycles


class TestCyclesWrong(unittest.TestCase):
    """Asserting an error is raised when cycles is not an int"""

    def test_cycles_wrong(self):
        with self.assertRaises(TypeError):
            sucyclesWrong = StorageUnit(name='su_cyclesWrong', time=TimeUnit(
                periods=4, dt=1), cycles=True)


class TestCycles(unittest.TestCase):
    def setUp(self):
        self.sucycles = StorageUnit(name='su_cycles', time=TimeUnit(
            periods=25, dt=1), cycles=12)

    def test_cycles(self):
        """Checking the cycles constraint is properly set"""
        self.assertIsInstance(self.sucycles.set_cycles,
                              TechnicalDynamicConstraint)
        self.assertEqual(self.sucycles.set_cycles.name, 'set_cycles')
        self.assertEqual(self.sucycles.set_cycles.t_range, 'for t in time.I['
                                                           ':-12]')
        self.assertEqual(self.sucycles.set_cycles.exp_t, '{0}_e[t] == {0}_e['
                                                         't+{1}]'.format(
            'su_cycles', 12))


class TestMinCapacity(unittest.TestCase):
    def setUp(self):
        self.suMinCapa = StorageUnit(name='su_min_capa', time=TimeUnit(
            periods=4, dt=1))
        self.suMinCapa.minimize_capacity()

    def test_def_capacity(self):
        """Checking the def_capacity constraint is properly defined"""
        self.assertIsInstance(self.suMinCapa.def_capacity, DynamicConstraint)
        self.assertEqual(self.suMinCapa.def_capacity.name, 'def_capacity')
        self.assertEqual(self.suMinCapa.def_capacity.t_range, 'for t in '
                                                              'time.I')
        self.assertEqual(self.suMinCapa.def_capacity.exp_t, '{0}_e[t] <= {'
                                                            '0}_capacity'
                         .format('su_min_capa'))

    def test_min_capacity(self):
        """Checking the min_capacity objective is properly defined"""
        self.assertIsInstance(self.suMinCapa.min_capacity, Objective)
        self.assertEqual(self.suMinCapa.min_capacity.name, 'min_capacity')
        self.assertEqual(self.suMinCapa.min_capacity.weight, 1)
        self.assertEqual(self.suMinCapa.min_capacity.exp,
                         '{0}_capacity'.format('su_min_capa'))


class TestThermoclineStorageSocMax(unittest.TestCase):
    def setUp(self):
        self.ts = ThermoclineStorage(name='ts', time=TimeUnit(
            periods=250, dt=1))
        self.dict_opt = {}
        for i in range(0, 250):
            self.dict_opt[i] = True

    def test_is_soc_max(self):
        """Checking is_soc_max is properly defined"""
        self.assertEqual(self.ts.is_soc_max.name, 'is_soc_max')
        self.assertEqual(self.ts.is_soc_max.opt, self.dict_opt)
        self.assertEqual(self.ts.is_soc_max.vlen, 250)
        self.assertEqual(self.ts.is_soc_max.vtype, LpBinary)
        self.assertEqual(self.ts.is_soc_max.description, 'indicates if the '
                                                         'storage is fully '
                                                         'charged 0:No 1:Yes')

    def test_is_soc_max_inf(self):
        """Checking is_soc_max_inf constraint is properly defined"""
        self.assertIsInstance(self.ts.def_is_soc_max_inf, DynamicConstraint)
        self.assertEqual(self.ts.def_is_soc_max_inf.name, 'def_is_soc_max_inf')
        self.assertEqual(self.ts.def_is_soc_max_inf.t_range, 'for t in time.I')
        self.assertEqual(self.ts.def_is_soc_max_inf.exp_t, '{0}_capacity * {'
                                                           '0}_is_soc_max[t] '
                                                           '>= ({0}_e[t] - '
                                                           '{0}_capacity + {1})'.format(
            'ts', 0.1))

    def test_is_soc_max_sup(self):
        """Checking is_soc_max_sup constraint is properly defined"""
        self.assertIsInstance(self.ts.def_is_soc_max_sup, DynamicConstraint)
        self.assertEqual(self.ts.def_is_soc_max_sup.name, 'def_is_soc_max_sup')
        self.assertEqual(self.ts.def_is_soc_max_sup.t_range, 'for t in time.I')
        self.assertEqual(self.ts.def_is_soc_max_sup.exp_t, '{0}_capacity * {'
                                                           '0}_is_soc_max[t] '
                                                           '<= {0}_e[t]'
                         .format('ts'))

    def test_force_soc_max(self):
        """Checking the force_soc_max constraint is properly defined"""
        self.assertIsInstance(self.ts.force_soc_max, DynamicConstraint)
        self.assertEqual(self.ts.force_soc_max.name, 'force_soc_max')
        self.assertEqual(self.ts.force_soc_max.t_range, 'for t in time.I['
                                                        '120:]')
        self.assertEqual(self.ts.force_soc_max.exp_t, 'lpSum({0}_is_soc_max['
                                                      'k] for k in range(t-{'
                                                      '1}+1, t)) >= 1'
                         .format('ts', 120))
