#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
..
    Copyright 2018 G2Elab / MAGE
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
         http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""


from omegalpes.general.optimisation.core import OptObject


class TURPE5(OptObject):
    def __init__(self, time, subscribed_power=[0, 0, 0, 0, 0],
                 voltage_level='HTB2', use='CU', peak='fixed'):
        """
        :param subscribed_power:

        :param voltage_level:
            HTB3 : 350 kV < Un <= 500 kV
            HTB2 : 130 kV < Un <= 350 kV
            HTB1 :  50 kV < Un <= 130 kV
            HTA2 :  40 kV < Un <=  50 kV
            HTA1 :   1 kV < Un <=  40 kV
        :param use:
            CU : Short use
            MU : Medium use
            LU : Long use
        :param peak:
            fixed :
            movable :
        """
        OptObject.__init__(self, name='TURPE5')
        non_sorted_power = subscribed_power
        subscribed_power.sort()
        if non_sorted_power != subscribed_power:
            raise ValueError("The subscribed power of a period should greater "
                             "or equal to the subscribed power of the previous "
                             "period.")

        if voltage_level == 'HTB3':
            c = 0.32

        elif voltage_level == 'HTB2':
            if use == 'CU':
                b = [0.87, 0.78, 0.75, 0.68, 0.37]
                c = [1.38, 0.87, 0.87, 0.68, 0.54]
            elif use == 'MU':
                b = [4.47, 4.27, 4.24, 3.37, 2.11]
                c = [1.16, 0.87, 0.62, 0.48, 0.30]
            elif use == 'LU':
                b = [12.13, 11.65, 9.68, 7.54, 3.73]
                c = [0.83, 0.61, 0.43, 0.28, 0.21]

        elif voltage_level == 'HTB1':
            if use == 'CU':
                b = [2.40, 2.00, 1.84, 1.10, 0.60]
                c = [2.34, 1.90, 1.58, 1.24, 0.89]
            elif use == 'MU':
                b = [18.03, 17.29, 14.32, 9.69, 4.54]
                c = [1.71, 1.36, 0.79, 0.58, 0.39]
            elif use == 'LU':
                b = [30.70, 29.71, 23.94, 17.08, 8.80]
                c = [1.40, 1.03, 0.61, 0.39, 0.14]

        elif voltage_level == 'HTA1':
            if use == 'CU':
                if peak == 'fixed':
                    b = [2.56, 2.29, 1.94, 1.76, 0.92]
                    c = [2.99, 2.82, 2.03, 1.88, 1.14]
                elif peak == 'movable':
                    b = [3.13, 2.20, 1.94, 1.76, 0.92]
                    c = [3.99, 2.7, 2.03, 1.88, 1.14]

            elif use == 'LU':
                if peak == 'fixed':
                    b = [15.7, 15.16, 12.79, 8.42, 1.61]
                    c = [2.74, 2.06, 1.28, 0.95, 0.84]
                elif peak == 'movable':
                    b = [18.04, 16.77, 12.79, 8.42, 1.61]
                    c = [3.13, 1.89, 1.28, 0.95, 0.84]

        # Peak hours
        period_1 = time.get_working_dates(month_range=[1, 2, 12],
                                          hour_range=[9, 10, 18, 19])

        # High season - high hours
        period_2 = time.get_working_dates(month_range=[1, 2, 12],
                                          hour_range=[7, 8, 11, 12, 13, 14,
                                                      15, 16, 17, 20, 21, 22])
        period_2 += time.get_working_dates(month_range=[3, 11],
                                           hour_range=range(7, 23))

        # High season - low hours
        period_3 = time.get_working_dates(month_range=[1, 2, 3, 11, 12],
                                          hour_range=list(range(7)) + [23])
        period_3 += time.get_non_working_dates(month_range=[1, 2, 3, 11, 12],
                                               hour_range=range(24))

        # Low season - high hours
        period_4 = time.get_working_dates(month_range=range(4, 11),
                                          hour_range=range(7, 23))

        # Low season - low hours
        period_5 = time.get_working_dates(month_range=range(4, 11),
                                          hour_range=list(range(7)) + [23])
        period_5 += time.get_non_working_dates(month_range=range(4, 11),
                                               hour_range=range(24))

        self.periods = [period_1, period_2, period_3, period_4, period_5]

        if voltage_level in ['HTA1', 'HTB1', 'HTB2']:
            energy_price = []
            for date in time.DATES:
                if date in period_1:
                    energy_price.append(c[0] / 100)
                elif date in period_2:
                    energy_price.append(c[1] / 100)
                elif date in period_3:
                    energy_price.append(c[2] / 100)
                elif date in period_4:
                    energy_price.append(c[3] / 100)
                elif date in period_5:
                    energy_price.append(c[4] / 100)
        elif voltage_level == 'HTB3':
            energy_price = c / 100

        self.energy_price = energy_price
        self.yearly_subscription_cost = b[0] * subscribed_power[0] \
                                        + sum(b[k] * (subscribed_power[k]
                                                      - subscribed_power[k - 1])
                                              for k in range(1, 5))

        self.subscription_cost = self.yearly_subscription_cost * time.DT * \
                                 time.LEN / 8760
